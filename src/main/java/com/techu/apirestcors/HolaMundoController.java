package com.techu.apirestcors;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", methods={RequestMethod.GET, RequestMethod.POST})
public class HolaMundoController {

    @GetMapping("/saludos")
    public String saludar(){
        return "Hola CORS!";
    }
}
